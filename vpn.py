import struct
import subprocess
from fcntl import ioctl

TUNSETIFF = 0x400454ca
TUNSETOWNER = TUNSETIFF + 2
IFF_TUN = 0x0001
IFF_NO_PI = 0x1000


def create_tun():
    tun = open('/dev/net/tun', 'rb')
    ifr = struct.pack('16sH', b'tun0', IFF_TUN | IFF_NO_PI)
    ioctl(tun, TUNSETIFF, ifr)
    subprocess.check_call(
        'ifconfig tun0 192.168.137.1 192.168.137.10 up', shell=True)
    return tun


if __name__ == '__main__':
    tun = create_tun()
    packet = list(os.read(tun.fileno(), 2048))
    os.write(tun.fileno(), ''.join(packet))
